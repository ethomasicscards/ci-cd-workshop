# Setup GIT repository

In this exercise a new git repository is setup and this project forked into the newly created repository. This will create the capability to create a continuous delivery pipeline tied to the newly created account using the Gitlab CI/CD capabilities.

## Create an account

This step can be skipped if an GitLab account already exists. Take these steps forcreating a GitLab account:

1. Go to the [GitLab sign-up page](https://gitlab.com/users/sign_in) and select register. ![Signup](images/signup.png)
2. Fill in the form with correct credentials and a confirmation e-mail will be sent.
3. Confirm the account by clicking the confirm link in the e-mail.
4. Now sign in with the newly create account on the [GitLab sign-up page](https://gitlab.com/users/sign_in) and select sign-in.

## Fork the project

Forking is a means of creating a copy of a project where a project can be extended and reshaped without touching the original project. Take these steps to fork the project:

1. Go to [CI/CD workshop project](https://gitlab.com/mtishauser/ci-cd-workshop)
2. Select fork button and select the newly created account. ![Fork](images/fork.png)
3. The project will be forked and will be available within one minute
