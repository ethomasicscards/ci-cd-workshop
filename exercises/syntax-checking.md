# Syntax checking

Syntax checking is a very fast way to validate whether the set syntax rules for a team/organisation are met.

## Goals

Have a mechanism in place to fail-fast when syntax is not correct or the code is not compliant.

## Approach

Validating syntax can be done by linting frameworks and/or checkstyle setups. By doing so a fail-fast mechanism allows 
for fast feedback whether to code is compliant with team rules.

## Exercise

In this exercise the linting is done by the eslint framework that can be used in combination with the npm build 
tool/package manager. In the [package.json](../package.json) is a step **lint** defined. This will call the eslint 
framework that validates the JavaScript code style.

### Step 1: disable sample stage and job

The sample stage and job are an example for minimal CI/CD pipeline setup. The stage can be disabled by adding a pound 
before the stage name, by replacing the contents of [.gitlab-ci.yml](../.gitlab-ci.yml) file.

```yaml
# Default docker image
image: node:10

# GitLab CI settings
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
  - node_modules/

variables:
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2

before_script:
  - npm install

# Stages

stages:
# Disable
#  - sample


# Jobs

# Disable with .
.sample:
  stage: sample
  script:
   - echo 'sample'
```

### Step 2: Create a linting step and stage

Create a linting step and stage will be the first step in the continuous delivery pipeline. Add the following parts to 
the [.gitlabb-ci.yml](file) 

```yaml
stages:
# Disable
#  - sample
  - qa

linting:
  stage: qa
  script:
   - npm run lint

```

The stage **qa** is an new logical divider of steps within the continuous delivery pipeline. The job **linting** is 
actual command that starts the linting. 

