# CI/CD Onboarding workshop

This a first experience workshop on continuous delivery.

## Goals

Get familiar with CI/CD concepts by building a pipeline containing functionals/non-functional test, security validation 
and syntactical checks.

## Exercises

1. [Setup GIT](exercises/setup-git-exercise.md)
2. [Setup CI/CD environment](exercises/setup-cicd.md) 
3. [Syntax checking](exercises/syntax-checking.md)
4. [Code build and package validation](exercises/code-build.md)
5. [Functional testing](exercises/functional-testing.md)
6. [User acceptance testing](exercises/user-acceptance-testing.md)
7. [(Mock) deploy](exercises/mock-deploy.md)
8. [Smoke testing](exercises/smoke-test.md)
9. [Performance testing](exercises/performance-test.md)

## Editing files

This workshop is designed to only require a browser. When changing files this can be done using the gitlab editor.